//
//  ChatCellView.swift
//
//  Created by Evgeny Aleksandrov on 05.07.2018.

import Cocoa

class ChatCellView: NSTableCellView {

    @IBOutlet weak var iconView: CorneredView!
    @IBOutlet weak var nameLabel: NSTextField!
    @IBOutlet weak var badgeView: BadgeView!
    @IBOutlet weak var titleLabel: NSTextField!
    @IBOutlet weak var messageLabel: NSTextField!
    @IBOutlet weak var messageBadgeConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateLabel: NSTextField!

    func config(with chat: Chat) {
        badgeView.count = chat.badgeCount
        messageBadgeConstraint.priority = badgeView.count == 0 ? .defaultLow : .defaultHigh

        iconView.bgColor = NSColor(hashingStringId: chat.title)
        nameLabel.stringValue = chat.initials
        titleLabel.stringValue = chat.title
        messageLabel.stringValue = chat.lastMessageDisplayText
        dateLabel.stringValue = chat.lastMessageDate?.timeString(withSeconds: true) ?? ""
    }
}

class ChatRowView: NSTableRowView {

    override func drawSelection(in dirtyRect: NSRect) {
        if self.selectionHighlightStyle != .none {
            NSColor(red: 225/255, green: 242/255, blue: 255/255, alpha: 1).setFill()
            let selectionPath = NSBezierPath(rect: bounds)
            selectionPath.fill()
        }
    }

    override var isEmphasized: Bool {
        set {}
        get {
            return false
        }
    }
}
