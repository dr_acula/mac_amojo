//
//  BadgeView.swift
//
//  Created by Evgeny Aleksandrov on 05.07.2018.

import Cocoa

@IBDesignable class BadgeView: CorneredView {

    @IBOutlet weak var badgeLabel: NSTextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        configureVisibility()
    }

    var count: Int = 0 {
        didSet {
            badgeLabel.stringValue = String(count)
            configureVisibility()
        }
    }

    private func configureVisibility() {
        isHidden = count <= 0
    }

    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        addSubviewFromNib()
    }

    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        addSubviewFromNib()
    }
}

extension BadgeView {

    static var randomCount: Int {
        return arc4random_uniform(3) != 0 ? 0 : Int(arc4random_uniform(15))
    }
}
