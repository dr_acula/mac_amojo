//
//  CorneredView.swift
//
//  Created by Evgeny Aleksandrov on 05.07.2018.

import Cocoa

class CorneredView: ColoredView {

    override func layout() {
        super.layout()

        roundCorners()
    }

    private func roundCorners() {
        guard let layer = layer else { return }
        let layerBounds = layer.bounds
        let radius = layerBounds.size.height / 2
        let path = NSBezierPath(roundedRect: layerBounds, xRadius: radius, yRadius: radius)
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
