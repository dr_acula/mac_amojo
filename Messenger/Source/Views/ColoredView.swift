//
//  ColoredView.swift
//
//  Created by Evgeny Aleksandrov on 05/07/2018.

import Cocoa

@IBDesignable class ColoredView: NSView {

    @IBInspectable var bgColor: NSColor? {
        didSet {
            configBg()
        }
    }

    @IBInspectable var makeGradient: Bool = false {
        didSet {
            configBg()
        }
    }

    private var mainLayer: CALayer? {
        if layer == nil {
            layer = CALayer()
        }
        return layer!
    }

    private func configBg() {
        if makeGradient {
            configGradientBg()
        } else {
            configPlainBg()
        }
    }

    private func configPlainBg() {
        mainLayer?.backgroundColor = bgColor?.cgColor
    }

    private func configGradientBg() {
        guard let bgColor = bgColor else { return }
        let complimentaryColor = NSColor(red: bgColor.redComponent/0.7, green: bgColor.greenComponent/0.7, blue: bgColor.blueComponent/0.7, alpha: 1)
        let gradientLayer = CAGradientLayer()

        gradientLayer.colors = [bgColor.cgColor, complimentaryColor.cgColor]
        gradientLayer.frame = bounds
        layer = gradientLayer
    }
}
