//
//  Chat+Mockable.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa
import LoremSwiftum

extension Chat: Mockable {

    static func generateInstance() -> Self {
        let title = Lorem.title
        let singleChatMessage = Message.generateInstance()
        return self.init(title: title, messages: [singleChatMessage])
    }
}
