//
//  ChatList+TableView.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa

extension ChatList: NSTableViewDataSource, NSTableViewDelegate {

    func numberOfRows(in tableView: NSTableView) -> Int {
        return chats.count
    }

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let cellIdentifierString = String(describing: ChatCellView.self)
        let cellIdentifier = NSUserInterfaceItemIdentifier(cellIdentifierString)
        let cell = tableView.makeView(withIdentifier: cellIdentifier, owner: nil) as? ChatCellView

        if chats.indices.contains(row) {
            let chat = chats[row]
            cell?.config(with: chat)
        }

        return cell
    }

    func tableView(_ tableView: NSTableView, rowViewForRow row: Int) -> NSTableRowView? {
        if indexIsReachingEnd(row), !reachedMaxSize {
            generateAndAppendChats()
            DispatchQueue.main.async {
                tableView.reloadData()
            }
        }

        return ChatRowView()
    }
}

private let kPreloadTriggerSize = 10

extension ChatList {

    func indexIsReachingEnd(_ index: Int) -> Bool {
        guard let lastIndex = chats.indices.last else { return false }
        return lastIndex - kPreloadTriggerSize <= index
    }
}
