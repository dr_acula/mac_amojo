//
//  User.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa

private let kCurrentUserFullname = "Mr. Cool"

class User: Model {

    static let current = User(fullName: kCurrentUserFullname)

    let fullName: String

    init(fullName: String) {
        self.fullName = fullName

        super.init()
    }
}

extension User: Equatable, Hashable {

    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.id == rhs.id
    }

    var hashValue: Int {
        return id.hashValue
    }
}
