//
//  Message.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa

class Message: Model {

    var text: String
    var sender: User
    var date: Date

    init(text: String, sender: User) {
        self.text = text
        self.sender = sender
        date = Date()

        super.init()
    }
}

//accessors
extension Message {
    
    var isSentByCurrentUser: Bool {
        return sender == User.current
    }
}

extension Message: Equatable, Hashable {

    static func == (lhs: Message, rhs: Message) -> Bool {
        return lhs.id == rhs.id && lhs.text == rhs.text && lhs.sender == rhs.sender && lhs.date == rhs.date
    }

    var hashValue: Int {
        return id.hashValue ^ text.hashValue ^ sender.hashValue ^ date.hashValue &* 16777619
    }
}
