//
//  User+Mockable.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa
import LoremSwiftum

extension User: Mockable {

    static func generateInstance() -> Self {
        let fullName = [Lorem.firstName, Lorem.lastName].joined(separator: " ")
        return self.init(fullName: fullName)
    }
}
