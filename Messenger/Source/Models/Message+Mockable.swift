//
//  Message+Mockable.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa
import LoremSwiftum

extension Message: Mockable {

    static func generateInstance() -> Self {
        let text = Lorem.sentence

        let user = arc4random_uniform(3) != 0 ? User.generateInstance() : User.current
        
        return self.init(text: text, sender: user)
    }
}
