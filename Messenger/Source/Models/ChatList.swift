//
//  ChatList.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa
import Differ

private let kMaxChats = 10000
private let kChatsBatchSizeInitial = 510
private let kChatsBatchSizeAdditional = 50

protocol ChatListDelegate: AnyObject {
    func chatListDidUpdate(updates: IndexSet)
    func chatListDidSort(additions: IndexSet, deletions: IndexSet, moves: [(Int, Int)])
}

class ChatList: NSObject {

    var type: ChatListType
    private(set) var chats = [Chat]()

    weak var delegate: ChatListDelegate?

    private var timer: Timer?
    private var generationDateOffset = Date().addingTimeInterval(-65)

    init(type: ChatListType) {
        self.type = type
        super.init()

        generateAndAppendChats(initial: true)
        setupAutoupdate()
    }
}

extension ChatList {

    var reachedMaxSize: Bool {
        return chats.count >= kMaxChats
    }

    func generateAndAppendChats(initial: Bool = false) {
        guard chats.count <= kMaxChats else { return }
        let batchSize = initial ? kChatsBatchSizeInitial : kChatsBatchSizeAdditional
        let batchSizeToAdd = min(kMaxChats - chats.count, batchSize)
        let chatsBatch = (0 ..< batchSizeToAdd).map { _ -> Chat in
            let newChat = Chat.generateInstance()
            newChat.lastMessage?.date = generationDateOffset
            generationDateOffset = generationDateOffset.addingTimeInterval(-65)
            return newChat
        }

        chats.append(contentsOf: chatsBatch)
    }

    func setupAutoupdate() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ChatList.updateData), userInfo: nil, repeats: true)
    }

    @objc func updateData() {
        guard chats.count > 0 else { return }

        var updatedIndexes: Set<Int> = []
        for _ in 0...(arc4random_uniform(3) + 2) {
            let idx = Int(arc4random_uniform(500))
            let chatToUpdate = chats[idx]
            chatToUpdate.addNewMessage()
            updatedIndexes.insert(idx)
        }
        for _ in 0...30 {
            let idx = Int(arc4random_uniform(500))
            let chatToUpdate = chats[idx]
            if chatToUpdate.badgeCount > 0 {
                chatToUpdate.markAsRead()
                updatedIndexes.insert(idx)
            }
        }

        if type == ChatListType.lastOpened {
            delegate?.chatListDidUpdate(updates: IndexSet(updatedIndexes))
        } else {
            // will be reloaded on tab switch
        }

        self.sortData()
    }

    @objc func sortData() {
        let previousState = type == ChatListType.lastOpened ? chats : []

        if arc4random_uniform(4) == 0 {
            for idx in 0...Int(arc4random_uniform(2)) {
                let newChat = Chat.generateInstance()
                chats.insert(newChat, at: idx)
            }
        }

        chats.sort(by: {
            if type == .clients, $0.badgeCount != $1.badgeCount {
                return $0.badgeCount > $1.badgeCount
            }
            return $0.lastMessageDate! > $1.lastMessageDate!
        })

        if type == ChatListType.lastOpened {
            let update = BatchUpdate(diff: previousState.extendedDiff(chats))

            delegate?.chatListDidSort(additions: IndexSet(update.insertions.map { $0.item }),
                                      deletions: IndexSet(update.deletions.map { $0.item }),
                                      moves: update.moves.map {($0.from.item, $0.to.item)})
        }

    }
}
