//
//  Chat.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa

private let kInitialsMaxLength = 2

class Chat: Model {

    private(set) var title: String
    private(set) var initials: String
    private(set) var messages: [Message]
    private(set) var badgeCount: Int

    init(title: String, messages: [Message], badgeCount: Int = 0) {
        self.title = title
        self.initials = title.getInitialsStringForWordsCount(kInitialsMaxLength)
        self.messages = messages
        self.badgeCount = badgeCount

        super.init()
    }
}

extension Chat: Equatable, Hashable {

    static func == (lhs: Chat, rhs: Chat) -> Bool {
        return lhs.id == rhs.id && lhs.messages.first == rhs.messages.first
    }

    var hashValue: Int {
        return id.hashValue ^ (messages.first?.hashValue ?? 0) &* 16777619
    }
}

extension Chat {

    // MARK: - Accessors

    var lastMessage: Message? {
        return messages.last
    }

    var lastMessageText: String? {
        return lastMessage?.text
    }

    var lastMessageDisplayText: String {
        guard let message = lastMessage else { return "" }
        var textComponents = [message.text]
        if !message.isSentByCurrentUser {
            textComponents.insert(message.sender.fullName, at: 0)
        }

        return textComponents.joined(separator: ": ")
    }

    var lastMessageDate: Date? {
        return lastMessage?.date
    }

    // MARK: - Actions

    func addNewMessage() {
        messages = [Message.generateInstance()]
        if lastMessage?.isSentByCurrentUser == true {
            markAsRead()
        } else {
            addUnread()
        }
    }

    func markAsRead() {
        badgeCount = 0
    }

    func addUnread() {
        badgeCount += 1
    }
}
