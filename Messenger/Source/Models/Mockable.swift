//
//  Mockable.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa

protocol Mockable {

    static func generateInstance() -> Self
}
