//
//  AppDelegate.swift
//
//  Created by Evgeny Aleksandrov on 05/07/2018.

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

}

