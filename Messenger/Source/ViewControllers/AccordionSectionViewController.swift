//
//  AccordionSectionViewController.swift
//
//  Created by Evgeny Aleksandrov on 05/07/2018.

import Cocoa

protocol AccordionSectionViewControllerDelegate: AnyObject {
    func accordionSectionViewControllerDidToggleExpand(controller: AccordionSectionViewController)
}

class AccordionSectionViewController: NSViewController {

    var chatListType = ChatListType.team

    weak var delegate: AccordionSectionViewControllerDelegate?
    private weak var chatListVC: ChatListViewController?
    @IBOutlet private weak var collapsedHeightConstraint: NSLayoutConstraint!

    // MARK: - IBOutlets

    @IBOutlet weak var headerLabel: NSTextField! {
        didSet {
            switch chatListType {
            case .team:
                headerLabel.stringValue = "Team"
            case .channels:
                headerLabel.stringValue = "Channels"
            case .clients:
                headerLabel.stringValue = "Clients"
                headerLabel.textColor = NSColor(hex: "3C6FEE")
            }
        }
    }
    @IBOutlet weak var headerView: ColoredView! {
        didSet {
            headerView.bgColor = NSColor.white
        }
    }

    // MARK: - Actions

    @IBAction func headerClick(_ sender: Any) {
        delegate?.accordionSectionViewControllerDidToggleExpand(controller: self)
    }

    // MARK: - Collapse/expand

    func setCollapsed(_ collapsed: Bool) {
        collapsedHeightConstraint.priority = collapsed ? .defaultHigh : .defaultLow
        if !collapsed {
            chatListVC?.changedFrame(expanded: !collapsed)
        }
    }

    var collapsed: Bool {
        return collapsedHeightConstraint.priority > .defaultLow
    }
}

extension AccordionSectionViewController: StoryboardSceneBased {
    static var sceneStoryboardName: String {
        return "Accordion"
    }
}

private let kChatListEmbedSegueId = "embedChatList"

extension AccordionSectionViewController {

    override func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        guard segue.identifier?.rawValue == kChatListEmbedSegueId,
            let chatListViewController = segue.destinationController as? ChatListViewController else { return }
        chatListViewController.chatListType = chatListType
        chatListVC = chatListViewController
    }
}
