//
//  AccordionViewController.swift
//
//  Created by Evgeny Aleksandrov on 05/07/2018.

import Cocoa

class AccordionViewController: NSViewController {

    // MARK: - IBOutlets

    @IBOutlet weak var sectionsStackView: NSStackView!

    private var sectionControllers = NSHashTable<AccordionSectionViewController>.weakObjects()

    // MARK: - View

    override func viewDidLoad() {
        super.viewDidLoad()

        populateWithSections()
    }

    // MARK: - Private actions

    private func animateSwitch() {
        NSAnimationContext.runAnimationGroup({ context in
            context.duration = 0.25
            context.allowsImplicitAnimation = true

            view.layoutSubtreeIfNeeded()
        }, completionHandler: nil)
    }
}

extension AccordionViewController: AccordionSectionViewControllerDelegate {

    func accordionSectionViewControllerDidToggleExpand(controller: AccordionSectionViewController) {
        let collapsed = controller.collapsed
        guard collapsed else { return }
        ChatListType.lastOpened = controller.chatListType

        let shouldCollapse = !collapsed
        controller.setCollapsed(shouldCollapse)

        let restControllers: [AccordionSectionViewController] = sectionControllers.objectEnumerator().filter {
            guard let restController = $0 as? AccordionSectionViewController else { return false }
            return restController != controller
        } as! [AccordionSectionViewController]

        restControllers.forEach {
            $0.setCollapsed(!shouldCollapse)
        }

        animateSwitch()
    }
}

enum ChatListType {
    case team
    case channels
    case clients

    static var lastOpened = ChatListType.channels

    static let all: [ChatListType] = [.team, .channels, .clients]

    func createViewController() -> AccordionSectionViewController {
        let viewController = AccordionSectionViewController.instantiate()
        viewController.chatListType = self
        return viewController
    }
}

extension AccordionViewController {

    private func populateWithSections() {
        sectionsStackView.removeAllArrangedSubviews()
        sectionControllers.removeAllObjects()
        ChatListType.all.forEach {
            let viewController = $0.createViewController()
            viewController.delegate = self
            add(childViewController: viewController, to: sectionsStackView)
            viewController.view.widthAnchor.constraint(equalTo: sectionsStackView.widthAnchor).isActive = true
            sectionControllers.add(viewController)
            let collapsed = $0 != ChatListType.lastOpened
            viewController.setCollapsed(collapsed)
        }
    }
}
