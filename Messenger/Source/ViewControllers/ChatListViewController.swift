//
//  ChatListViewController.swift
//
//  Created by Evgeny Aleksandrov on 05.07.2018.

import Cocoa

class ChatListViewController: NSViewController {

    @IBOutlet private weak var tableSingleColumn: NSTableColumn!
    @IBOutlet private weak var tableView: NSTableView! {
        didSet {
            chatList = ChatList(type: chatListType)
            chatList?.delegate = self
            tableView.dataSource = chatList
            tableView.delegate = chatList
        }
    }

    var chatList: ChatList?
    var chatListType = ChatListType.team

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.columnAutoresizingStyle = .uniformColumnAutoresizingStyle
        tableSingleColumn.resizingMask = .autoresizingMask
    }

    override func viewDidLayout() {
        super.viewDidLayout()

        tableView.sizeLastColumnToFit()
    }

    func changedFrame(expanded: Bool) {
        if expanded {
            tableView.reloadData()
        }
    }
}

extension ChatListViewController: ChatListDelegate {

    func chatListDidUpdate(updates: IndexSet) {
        DispatchQueue.main.async {
            self.tableView.reloadData(forRowIndexes: updates, columnIndexes: [0])
        }
    }

    func chatListDidSort(additions: IndexSet, deletions: IndexSet, moves: [(Int, Int)]) {

        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            self.tableView.removeRows(at: additions, withAnimation: .slideDown)
            self.tableView.insertRows(at: additions, withAnimation: .slideDown)
            moves.forEach {
                self.tableView.moveRow(at: $0.0, to: $0.1)
            }
            self.tableView.endUpdates()
        }
    }

}
