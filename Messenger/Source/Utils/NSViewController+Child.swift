//
//  NSViewController+Child.swift
//
//  Created by Evgeny Aleksandrov on 06.07.2018.

import Cocoa

extension NSViewController {

    func add(childViewController: NSViewController, to container: NSView) {
        add(childViewController: childViewController) {
            childViewController.view.frame = container.bounds
            container.addSubview(childViewController.view)
        }
    }

    func add(childViewController: NSViewController, to stackViewContainer: NSStackView) {
        add(childViewController: childViewController) {
            stackViewContainer.addArrangedSubview(childViewController.view)
        }
    }

    private func add(childViewController: NSViewController, addSubviewBlock: () -> Void) {
        addChildViewController(childViewController)
        addSubviewBlock()
    }

    func remove(childViewController: NSViewController) {
        childViewController.removeFromParentViewController()
    }
}
