//
//  NSStoryboard+Identifier.swift
//
//  Created by Evgeny Aleksandrov on 06.07.2018.

import Cocoa

public protocol StoryboardSceneBased: class {
    static var sceneStoryboardName: String { get }
    static var sceneIdentifier: NSStoryboard.SceneIdentifier { get }
}

public extension StoryboardSceneBased {
    static var sceneIdentifier: NSStoryboard.SceneIdentifier {
        let rawValue = String(describing: self)
        return NSStoryboard.SceneIdentifier(rawValue)
    }
}

public extension StoryboardSceneBased where Self: NSViewController {
    static func instantiate() -> Self {
        let storyboardName = NSStoryboard.Name(rawValue: Self.sceneStoryboardName)
        let storyboard = NSStoryboard(name: storyboardName, bundle: nil)
        guard let vc = storyboard.instantiateController(withIdentifier: self.sceneIdentifier) as? Self else {
            fatalError("The viewController '\(self.sceneIdentifier)' of '\(storyboard)' is not of class '\(self)'")
        }
        return vc
    }
}
