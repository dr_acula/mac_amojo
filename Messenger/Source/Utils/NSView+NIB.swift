//
//  NSView+NIB.swift
//
//  Created by Evgeny Aleksandrov on 05.07.2018.

import Cocoa

extension NSView {

    func viewFromNib() -> NSView? {
        let nibNameString = String(describing: type(of: self))
        let nibName = NSNib.Name(rawValue: nibNameString)
        var subviews: NSArray?
        Bundle.main.loadNibNamed(nibName, owner: self, topLevelObjects: &subviews)
        return subviews?.filter { type(of: $0) == NSView.self }.first as? NSView
    }

    func addSubviewFromNib() {
        guard let view = viewFromNib() else { return }
        view.frame = bounds
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        
        view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        view.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        view.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
}
