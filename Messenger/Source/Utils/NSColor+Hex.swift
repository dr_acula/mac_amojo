//
//  NSColor+Hex.swift
//
//  Created by Evgeny Aleksandrov on 06/07/2018.

import Cocoa

extension NSColor {

    convenience init(hex: String, alpha: CGFloat = 1) {
        let scanner = Scanner(string: hex)

        var rgb: UInt32 = 0
        scanner.scanHexInt32(&rgb)

        self.init(
            red:   CGFloat((rgb & 0xFF0000) >> 16)/255.0,
            green: CGFloat((rgb &   0xFF00) >>  8)/255.0,
            blue:  CGFloat((rgb &     0xFF)      )/255.0,
            alpha: alpha)
    }

    convenience init(hashingStringId stringId: String) {
        let hexString = convertToRGB(from: customHash(from: stringId))
        self.init(hex: hexString)
    }
}

private func customHash(from string: String) -> UInt64 {
    var hash: UInt64 = 0
    for char in string.utf16 {
        hash = (UInt64(char) + ((hash << 5)  - hash)) & 0x00FFFFFF
    }
    return hash
}

private func convertToRGB(from int: UInt64) -> String {
    let str = String(int, radix: 16, uppercase: true)
    return String(repeating: "0", count: 6 - str.count) + str
}
