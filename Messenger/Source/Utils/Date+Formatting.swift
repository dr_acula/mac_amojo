//
//  Date+Formatting.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa

extension Date {

    func timeString(withSeconds: Bool = false) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = withSeconds ? "HH:mm:ss" : "HH:mm"
        formatter.timeZone = TimeZone.current
        return formatter.string(from: self)
    }
}
