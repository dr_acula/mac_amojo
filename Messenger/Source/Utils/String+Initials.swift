//
//  String+Initials.swift
//
//  Created by Evgeny Aleksandrov on 09.07.2018.

import Cocoa

extension String {

    func getInitialsStringForWordsCount(_ wordsCount: Int) -> String {
        let components = split(separator: " ")
        let componentsForInitials = components.prefix(wordsCount)
        return componentsForInitials.map { $0.prefix(1) }.joined()
    }
}
