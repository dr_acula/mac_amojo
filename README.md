# MacOS messenger app

Demo of handling big live (mocked) lists of channels.

## How To Build

### Requirements

Swift 4, macOS deployment target: 10.13.

### Setup

1. First, install Carthage: `gem install carthage`.
2. Then, to install all dependencies, run: `carthage bootstrap`.
